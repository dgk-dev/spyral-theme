<?php
// Cleanup
require_once( trailingslashit( get_template_directory() ). '/includes/cleanup.php' );

// Resources
require_once( trailingslashit( get_template_directory() ). '/includes/resources.php' );

// Custom post types
require_once( trailingslashit( get_template_directory() ). '/includes/branch-customposttype.php' );
require_once( trailingslashit( get_template_directory() ). '/includes/slide-customposttype.php' );
require_once( trailingslashit( get_template_directory() ). '/includes/product-customposttype.php' );

// Map functions
require_once( trailingslashit( get_template_directory() ). '/includes/map-functions.php' );

// Settings
require_once( trailingslashit( get_template_directory() ). '/admin/settings.php' );

// Customize excerpt word count length
function custom_excerpt_length() {
	return 22;
}

add_filter( 'excerpt_length', 'custom_excerpt_length' );

show_admin_bar( false );

// Checks if there are any posts in the results
function is_search_has_results() {
	return 0 != $GLOBALS['wp_query']->found_posts;
}
