<?php
$args = array(
	'post_type'              => array( 'spyral-slide' ),
	'post_status'            => array( 'publish' ),
	'nopaging'               => true,
	'posts_per_page'         => '-1',
);
$query = new WP_Query( $args );
?>
<section class="slideshow swiper-container">
    <div class="slideshow__bg slideshow__bg--tl"></div>
    <div class="slideshow__bg slideshow__bg--tr"></div>
    <div class="slideshow__bg slideshow__bg--mr"></div>
    <div class="slideshow__bg slideshow__bg--br"></div>
    <div class="slideshow__bg slideshow__bg--bl"></div>
    <div class="slideshow__bg slideshow__bg--bl2"></div>

    <?php if ( $query->have_posts() ): ?>
        <div class="container swiper-wrapper slideshow__wrapper">
            <?php while ( $query->have_posts() ): $query->the_post(); ?>
                <?php
                    $imgDeskId = get_post_meta(get_the_ID(), 'slide-img-desk', true);
                    $imgMobId = get_post_meta(get_the_ID(), 'slide-img-mob', true);
                    $srcDesk = wp_get_attachment_url( $imgDeskId );
                    $srcMob = wp_get_attachment_url( $imgMobId );
                
                ?>
                <div class="flex swiper-slide slideshow__slide">
                    <div class="flex-space-between slideshow__image-container--full">
                        <figure class="slideshow__image slideshow__image--desk">
                            <img src="<?php echo $srcDesk; ?>" alt="Spyral - <?php echo get_the_title(); ?>">
                        </figure>
                        <figure class="slideshow__image slideshow__image--mobile">
                            <img src="<?php echo $srcMob; ?>" alt="Spyral - <?php echo get_the_title(); ?>">
                        </figure>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>

        <div class="swiper-pagination slideshow__pagination"></div>

        <div class="swiper-button-next slideshow__button slideshow__button--prev"></div>
        <div class="swiper-button-prev slideshow__button slideshow__button--next"></div>
    <?php endif; wp_reset_postdata(); ?>
</section>
<!-- Ends slideshow -->