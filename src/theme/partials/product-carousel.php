<?php
$args = array(
	'post_type'              => array( 'spyral-product' ),
	'post_status'            => array( 'publish' ),
	'nopaging'               => true,
	'posts_per_page'         => '-1',
);
$query = new WP_Query( $args );
?>

<section class="our-products" id="our-products">
    <div class="container">
        <hgroup class="section-title">
            <figure class="section-title__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="59" viewBox="0 0 13 27">
                    <g fill="#FFF" fill-rule="evenodd">
                        <path d="M6.6 22.02l-1.74-1.44-.96.84c.24.84.42 1.44.54 1.86l.42-.36 1.74 1.44 1.74-1.44.42.36c.18-.6.36-1.2.54-1.86l-.96-.84-1.74 1.44zM2.28 15.84l.9.72 1.74-1.44 1.74 1.44 1.74-1.44 1.74 1.44.84-.72c.24-.72.42-1.38.54-1.92H1.68c.24.72.42 1.38.6 1.92z"/>
                        <path d="M8.34 15.84L6.6 17.28l-1.74-1.44-1.74 1.44-.6-.54c.24.84.48 1.56.66 2.16l1.68-1.44L6.6 18.9l1.74-1.44 1.68 1.44c.24-.78.42-1.5.66-2.16l-.6.54-1.74-1.44z"/>
                        <path d="M6.6 19.62l-1.74-1.44L3.3 19.5c.12.48.3.96.42 1.38l1.14-.96 1.74 1.44 1.74-1.44 1.14.96c.12-.48.24-.9.42-1.38l-1.56-1.32-1.74 1.44zM6.6 24.96l-1.74-1.44-.3.24c.06.12 0-.06.48 1.62l1.5 1.26 1.5-1.26c.18-.54.3-1.08.48-1.68l-.3-.24-1.62 1.5zM2.22 11.7h9.06c1.74-.36 1.5-1.74 1.2-2.52-.12-.36-.36-.54-.54-.72-.9.84-2.34 1.56-4.44 1.74 1.98-.36 3.3-1.2 4.08-2.1-.54-.66-.42-1.26-.42-1.32.18-.66-.18-1.56-.78-2.04-.06 0-.06-.06-.06-.06-.9.96-2.28 1.68-4.2 1.8 1.5-.3 2.94-.96 3.84-2.22-.54-.78-.42-1.8-.48-2.04C8.82.3 6.54.48 5.88.6c-.12 0-.24.12-.24.3-.06.36.3.24.36.66.18.84-.6 1.02-.6 1.02-.72.12-1.02 1.38-1.08 1.44l-.06.06c.42.48.9.9 1.8 1.26-.78-.12-1.74-.48-2.34-1.08-.78.3-1.08 1.08-1.08 1.08-.42.6.12 1.38.18 1.8 1.02 1.2 2.46 1.62 3.9 1.74C5.1 9 3.48 8.7 2.34 7.44.78 7.74.78 9.3.72 9.36c-.3.96 0 1.56.36 1.92.24.3.66.42 1.14.42zM11.82 12.18H1.38a.601.601 0 0 0 0 1.2h10.38c.3 0 .6-.24.6-.6 0-.3-.24-.6-.54-.6z"/>
                    </g>
                </svg>
            </figure>
            <h2 class="section-title__text">Nuestros productos</h2>
        </hgroup>
    </div>
    <?php if ( $query->have_posts() ): ?>
        <div class="carousel swiper-container">
            <div class="flex-space-between carousel__wrapper swiper-wrapper">
                <?php while ( $query->have_posts() ): $query->the_post(); ?>
                    <?php
                        $title = get_the_title();
                        $description = get_the_content();
                        $mainImg = get_the_post_thumbnail_url();
                        $mainColor = get_post_meta(get_the_ID(), 'product-main-color', true);
                        $flavors = get_post_meta(get_the_ID(), 'product-flavors', true);
                        $footImgId = get_post_meta(get_the_ID(), 'product-foot-img', true);
                        $footImg = wp_get_attachment_url( $footImgId );
                        $hideTitle = get_post_meta(get_the_ID(), 'product-hide-title', true);
                        $carouselImgId = get_post_meta(get_the_ID(), 'product-carousel-img', true);
                        $carouselImg = wp_get_attachment_url( $carouselImgId );
                    
                    ?>
                    <div class="swiper-slide carousel__slide carousel__slide--<?php echo $mainColor; ?>">
                        <a 
                            href="#" 
                            data-text="<?php echo esc_attr($description); ?>"
                            data-list="<?php echo $flavors; ?>"
                            data-img="<?php echo $mainImg; ?>"
                            data-img-footer="<?php echo $footImg; ?>"
                            data-color="<?php echo $mainColor; ?>"
                        >
                            <?php if (!$hideTitle): ?>
                                <h3 class="carousel__title"><?php echo $title; ?></h3>
                            <?php endif; ?>
                            <figure class="carousel__image">
                                <img src="<?php echo $carouselImg; ?>" alt="Spyral - <?php echo $title; ?>">
                            </figure>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>

            <div class="carousel__controls">
                <div class="carousel__button carousel__button--prev">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="20" viewBox="0 0 12 20">
                        <path fill="#FFF" fill-rule="nonzero" d="M.445 8.56c-.28.283-.44.663-.445 1.06v.76c0 .398.16.78.445 1.06l8.381 8.264a1.018 1.018 0 0 0 1.437 0l1.438-1.4a.995.995 0 0 0 0-1.421L4.717 10l6.963-6.883a.975.975 0 0 0 0-1.4L10.243.296a1.018 1.018 0 0 0-1.437 0L.446 8.559z"/>
                    </svg>
                </div>
                <div class="carousel__button carousel__button--next">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="20" viewBox="0 0 12 20">
                        <path fill="#FFF" fill-rule="nonzero" d="M3.154 19.697a.977.977 0 0 1-1.417 0l-1.438-1.42a.99.99 0 0 1 0-1.4L7.283 10 .3 3.122a.994.994 0 0 1 0-1.42l1.438-1.4a.977.977 0 0 1 1.417 0l8.4 8.258c.285.281.446.663.446 1.06v.76a1.53 1.53 0 0 1-.445 1.06l-8.401 8.257z"/>
                    </svg>
                </div>
            </div>
        </div>
    <?php endif; wp_reset_postdata(); ?>
</section>
<!-- Ends our products -->