<?php get_header(); ?>
<!-- container -->
<div class="container">
	<!-- site-content -->
	<div class="site-content">
		<article class="page">
			<h1 class="page-title">404</h1>
			<br>
			<div class="page-content">
				<p>Parece que la página que estás buscando no existe.</p>
			</div>
		</article>
	</div>
	<!-- /site-content -->
</div>
<!-- /container -->
<?php get_footer(); ?>
