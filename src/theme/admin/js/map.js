(function ($) {
    function initialize() {
        var $branchLat = $('#branch-lat');
        var $branchLng = $('#branch-lng');
        var $searchInput = $('#map-search-input');
        var $searchButton = $('#map-search-button');

        var hlat = parseFloat($branchLat.val()) || 19.432608;
        var hlng = parseFloat($branchLng.val()) || -99.133209;

        var placeLatLng = new google.maps.LatLng(hlat, hlng);

        var mapOptions = {
            center: placeLatLng,
            zoom: 16
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker;
        placeMarker(placeLatLng);

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });

        $('.location-input').change(function () {
            hlat = parseFloat($branchLat.val());
            hlng = parseFloat($branchLng.val());
            if (hlat && hlng) {
                placeLatLng = new google.maps.LatLng(hlat, hlng);
                placeMarker(placeLatLng);
            }
        });

        $searchInput.on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                searchPlace();
            }
        });

        var geocoder = new google.maps.Geocoder();
        $searchButton.click(function (e) {
            e.preventDefault();
            searchPlace();
        });

        function placeMarker(location) {
            if (marker == undefined) {
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP
                });
            }
            else {
                marker.setPosition(location);
            }
            $branchLat.val(location.lat());
            $branchLng.val(location.lng());
        }

        function searchPlace(){
            if (!$searchInput.val()) return false;
            geocoder.geocode({ 'address': $searchInput.val() }, function (results, status) {
                if (status == 'OK') {
                    map.setCenter(results[0].geometry.location);
                    map.fitBounds(results[0].geometry.viewport);
                    placeMarker(results[0].geometry.location);
                }
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
})(jQuery);