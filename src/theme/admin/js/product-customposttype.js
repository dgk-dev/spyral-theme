(function ($) {
    $mainColorSelector = $('#product-main-color');
    $mainColorSample = $('#product-main-color-sample');

    $mainColorSelector.change(function(){
        var color = $(this).children("option:selected").val();
        $mainColorSample.removeAttr('class').addClass(color);
    }).change();
   
})(jQuery);