/**
        * Load media uploader on pages with our custom metabox
        */
jQuery(document).ready(function($){

    'use strict';

    // Instantiates the variable that holds the media library frame.
    var metaImageFrame;
    var $mediaSelectors = $('button.media-selector');

    // Runs when the media button is clicked.
    $mediaSelectors.click(function(e){

        var $this = $(this);

        // Get the field target
        var field = $this.data('media-uploader-target');
        var img = $this.data('img-target');

        // Prevents the default action from occuring.
        e.preventDefault();

        // Sets up the media library frame
        metaImageFrame = wp.media.frames.metaImageFrame = wp.media();

        // Runs when an image is selected.
        metaImageFrame.on('select', function(){
            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = metaImageFrame.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $(field).val(media_attachment.id);
            $(img).attr('src', media_attachment.url);
            setMediaRemove($this);
        });

        // Opens the media library frame.
        metaImageFrame.open();
    });

    $mediaSelectors.each(function(index){
        setMediaRemove(this);
    });

    $('body').on('click', '.media-remove', function(e){
        e.preventDefault();
        var $this = $(this);
        var field = $this.data('media-uploader-target');
        var img = $this.data('img-target');
        $(field).val('');
        $(img).attr('src', '');
        $this.remove();
    })

    function setMediaRemove(media){
        var $media = $(media);
        var $imgTarget = $($media.data('img-target'));
        if(!$imgTarget.next('a.media-remove').length && $imgTarget.attr('src') != ''){
            $imgTarget.after('<a class="media-remove" data-img-target="'+$media.data('img-target')+'" data-media-uploader-target="'+$media.data('media-uploader-target')+'" href="#">Eliminar imagen</a>');
        }
    }
});