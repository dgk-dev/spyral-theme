<?php

/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
*/


class SpyralSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
        add_action('admin_enqueue_scripts', array($this, 'add_assets'));
    }

    /**
     * Add JS and CSS
     */
    public function add_assets()
    {
        wp_enqueue_script('spyral-settings', get_template_directory_uri() . '/admin/js/settings.js', array('jquery'), null, true);
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        /*
	add_options_page(
            'Settings Admin', 
            'My Settings', 
            'manage_options', 
            'my-setting-admin', 
            array( $this, 'create_admin_page' )
        );
	*/

        // add top level menu page
        add_menu_page(
            'Configuración Spyral',
            'Opciones Spyral',
            'manage_options',
            'spyral-settings',
            array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option('spyral_settings');
    ?>
        <div class="wrap">
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('spyral_settings_group');
                do_settings_sections('spyral-settings');
                submit_button();
                ?>
            </form>
        </div>
    <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'spyral_settings_group', // Option group
            'spyral_settings', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'spyral_landing_settings_section', // ID
            'Opciones de la Landing Page Spyral', // Title
            array($this, 'print_section_info'), // Callback
            'spyral-settings' // Page
        );

        add_settings_field(
            'gmaps_apikey', // ID
            'Google Maps API Key', // Title 
            array($this, 'gmaps_apikey_callback'), // Callback
            'spyral-settings', // Page
            'spyral_landing_settings_section' // Section           
        );

        add_settings_field(
            'analytics_code', // ID
            'Google Tag Manager', // Title 
            array($this, 'analytics_code_callback'), // Callback
            'spyral-settings', // Page
            'spyral_landing_settings_section' // Section           
        );

        add_settings_field(
            'analytics_add_view_product_event', // ID
            'Evento view', // Title 
            array($this, 'analytics_add_view_product_event_callback'), // Callback
            'spyral-settings', // Page
            'spyral_landing_settings_section' // Section           
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['gmaps_apikey']))
            $new_input['gmaps_apikey'] = sanitize_text_field($input['gmaps_apikey']);

        if (isset($input['analytics_code']))
            $new_input['analytics_code'] = $input['analytics_code'];

        if (isset($input['analytics_add_view_product_event']))
            $new_input['analytics_add_view_product_event'] = $input['analytics_add_view_product_event'];

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        printf('<p>Opciones para el correcto funcionamiento de la Landing Page</p>');
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function gmaps_apikey_callback()
    {
    ?>
        <input style="width: 100%" type="text" id="gmaps_apikey" name="spyral_settings[gmaps_apikey]" value="<?php echo isset($this->options['gmaps_apikey']) ? esc_attr($this->options['gmaps_apikey']) : '' ?>" />

        <p class="description">Se requiere de una API Key de Google Maps (con Geocoding activado) para el funcionamiento del mapa de la Landing Page y del panel administrativo de Sucursales.</p>
    <?php
    }

    public function analytics_code_callback()
    {
    ?>
        <textarea style="width: 100%" name="spyral_settings[analytics_code]" id="analytics_code" cols="30" rows="10"><?php echo isset($this->options['analytics_code']) ? esc_attr($this->options['analytics_code']) : '' ?></textarea>
        <p class="description">Código de Google Analytics.</p>
    <?php
    }

    public function analytics_add_view_product_event_callback()
    {
    ?>
        <label for="analytics_add_view_product_event">
            <input type="checkbox" name="spyral_settings[analytics_add_view_product_event]" id="analytics_add_view_product_event" <?php echo isset($this->options['analytics_add_view_product_event']) && $this->options['analytics_add_view_product_event'] ? 'checked="checked"' : '' ?>> Activar
        </label>
        <p class="description">Activar evento "view" en Google Analytics para detalle de producto.</p>
    <?php
    }
}

if (is_admin())
    $spyral_settings = new SpyralSettings();
