<?php get_header(); ?>
    <?php get_template_part('partials/slideshow'); ?>
    
    <section class="detail">
    </section><!-- Ends details -->

    <section class="find-us" id="find-us">
        <div class="container">
            <hgroup class="section-title">
                <figure class="section-title__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="40" viewBox="0 0 16 20">
                        <path fill="#FFF" fill-rule="nonzero" d="M8 0a8 8 0 0 0-8 8c0 6.22 5.05 6.17 6.26 9.78l.51 1.54a1 1 0 0 0 1 .68h.56a1 1 0 0 0 1-.68l.51-1.54C11 14.17 16 14.22 16 8a8 8 0 0 0-8-8zm0 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
                    </svg>
                </figure>
                <h2 class="section-title__text">¿Dónde encontrarnos?</h2>
            </hgroup>

            <form id="spyral-map-search" novalidate>
                <div class="flex search-container">
                    <input type="text" name="location" id="location" class="form-control" placeholder="Ingresa la ciudad, municipio o estado" />
                    <button type="submit" class="btn btn--submit">Buscar</button>
                </div>
            </form>
        </div>

        <div class="find-us__map" id="spyral-map"></div>
    </section>
	<!-- Ends find us -->
	
	<?php get_template_part('partials/product-carousel'); ?>
<?php get_footer(); ?>