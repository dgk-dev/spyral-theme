<?php
// Custom post type
add_action('init', 'spyral_custom_post_type_product', 0);
function spyral_custom_post_type_product()
{
    $labels = array(
        'name'                => __('Productos'),
        'singular_name'       => __('Producto'),
        'menu_name'           => __('Productos'),
        'parent_item_colon'   => __('Producto padre'),
        'all_items'           => __('Todos los Productos'),
        'view_item'           => __('Ver Producto'),
        'add_new_item'        => __('Agregar Nuevo Producto'),
        'add_new'             => __('Agregar Producto'),
        'edit_item'           => __('Editar Producto'),
        'update_item'         => __('Actualizar Producto'),
        'search_items'        => __('Buscar Producto'),
        'not_found'           => __('Producto no encontrado'),
        'not_found_in_trash'  => __('Producto no encontrado en papelera')
    );
    $args = array(
        'label'               => __('spyral-product'),
        'description'         => __('Productos Spyral'),
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'thumbnail'),
        'public'              => false,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => false,
        'has_archive'         => false,
        'can_export'          => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'rewrite'             => array('slug' => 'producto'),
        'menu_icon'           => 'dashicons-tag'
    );
    register_post_type('spyral-product', $args);
}

// Add assets
function spyral_product_assets() {
    $screen = get_current_screen();
    if($screen->id == 'spyral-product'){
        wp_enqueue_style( 'product-style', get_template_directory_uri().'/admin/css/product-customposttype.css');
        wp_enqueue_script( 'product-script', get_template_directory_uri().'/admin/js/product-customposttype.js', null, 1.0, true);
        
        wp_enqueue_style( 'multiple-input-style', get_template_directory_uri().'/admin/css/multiple-input.css');
        wp_enqueue_script( 'multiple-input-script', get_template_directory_uri().'/admin/js/multiple-input.js', null, 1.0, true);
        wp_enqueue_script( 'media-selector-script', get_template_directory_uri().'/admin/js/media-selector.js');
    }
}
add_action( 'admin_enqueue_scripts', 'spyral_product_assets' );

// Create Metaboxes
function spyral_product_meta_boxes()
{
    add_meta_box(
        'product_details_meta_box', // $id
        'Detalle', // $title
        'spyral_product_details_meta_box', // $callback
        'spyral-product', // $page
        'normal', // $context
        'high'
    ); // $priority
    add_meta_box(
        'product_carousel_meta_box', // $id
        'Carrusel', // $title
        'spyral_product_carousel_meta_box', // $callback
        'spyral-product', // $page
        'normal', // $context
        'high'
    ); // $priority
}
add_action('add_meta_boxes', 'spyral_product_meta_boxes');

// Show Metabox Contents
function spyral_product_details_meta_box()
{
    global $post;
    $mainColor = get_post_meta( $post->ID, 'product-main-color', true );
    $flavors = get_post_meta( $post->ID, 'product-flavors', true );
    $imgFoot = get_post_meta( $post->ID, 'product-foot-img', true );
    $srcFoot = wp_get_attachment_url( $imgFoot );
    $nonce = wp_create_nonce(basename(__FILE__));
    ?>
    <input type="hidden" name="spyral_product_nonce" value="<?php echo $nonce; ?>">
    <table style="width: 100%;">
        <tbody class="form-table">
            <tr>
                <th>
                    <label for="product-main-color"><?php _e('Color principal', 'spyral-theme') ?></label>
                </th>
                <td>
                    <select name="product-main-color" id="product-main-color">
                        <option value=''>--Seleccionar color--</option>
                        <option <?php echo $mainColor == 'purple' ? 'selected' : '' ?> value="purple">Morado</option>
                        <option <?php echo $mainColor == 'green' ? 'selected' : '' ?> value="green">Verde</option>
                        <option <?php echo $mainColor == 'orange' ? 'selected' : '' ?> value="orange">Naranja</option>
                        <option <?php echo $mainColor == 'blue' ? 'selected' : '' ?> value="blue">Azul</option>
                        <option <?php echo $mainColor == 'yellow' ? 'selected' : '' ?> value="yellow">Amarillo</option>
                    </select>
                    <span id="product-main-color-sample"></span>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="product-flavors"><?php _e('Sabores', 'spyral-theme') ?></label>
                </th>
                <td>
                    <input type="text" class="multiple-input" name="product-flavors" value="<?php echo esc_attr($flavors); ?>">
                    <small>Agregar elementos dando enter</small>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="product-foot-img"><?php _e('Imagen Footer', 'spyral-theme') ?></label>
                </th>
                <td>
                    <input type="hidden" name="product-foot-img" id="product-foot-img" value="<?php echo esc_attr($imgFoot); ?>">
                    <button type="button" class="button media-selector" data-media-uploader-target="#product-foot-img" data-img-target="#thumbnail-product-foot-img"><?php _e('Elegir archivo', 'spyral-theme') ?></button>
                    <img style="max-width: 200px; margin: 0 0 10px 0; display:block;" id="thumbnail-product-foot-img" src="<?php echo esc_attr($srcFoot); ?>" alt="">
                </td>
            </tr>
        </tbody>
    </table>

    <?php
}

function spyral_product_carousel_meta_box()
{
    global $post;
    $hideTitle = get_post_meta( $post->ID, 'product-hide-title', true );
    $imgCarousel = get_post_meta( $post->ID, 'product-carousel-img', true );
    $srcCarousel = wp_get_attachment_url( $imgCarousel );
    ?>
    <table style="width: 100%;">
        <tbody class="form-table">
            <tr>
                <th>
                    <?php _e('Ocultar título en carrusel', 'spyral-theme') ?>
                </th>
                <td>
                    <label for="product-hide-title">
                        <input type="checkbox" name="product-hide-title" id="product-hide-title" <?php echo $hideTitle ? 'checked="checked"' : '' ?>>
                        Ocultar
                    </label>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="product-carousel-img"><?php _e('Imagen de carrusel', 'spyral-theme') ?></label>
                </th>
                <td>
                    <input type="hidden" name="product-carousel-img" id="product-carousel-img" value="<?php echo esc_attr($imgCarousel); ?>">
                    <button type="button" class="button media-selector" data-media-uploader-target="#product-carousel-img" data-img-target="#thumbnail-product-carousel-img"><?php _e('Elegir archivo', 'spyral-theme') ?></button>
                    <img style="max-width: 200px; margin: 0 0 10px 0; display:block;" id="thumbnail-product-carousel-img" src="<?php echo esc_attr($srcCarousel); ?>" alt="">
                </td>
            </tr>
        </tbody>
    </table>

    <?php
}

// // Save Metaboxes.
function spyral_product_save_data($post_id) {   
    // verify nonce
    if (!wp_verify_nonce($_POST['spyral_product_nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
    
    $oldmaincolor = get_post_meta($post_id, "product-main-color", true);
    $newmaincolor = $_POST["product-main-color"]; 
    if ($newmaincolor != $oldmaincolor) {
        update_post_meta($post_id, "product-main-color", $newmaincolor);
    }
    
    $oldflavors = get_post_meta($post_id, "product-flavors", true);
    $newflavors = $_POST["product-flavors"]; 
    if ($newflavors != $oldflavors) {
        update_post_meta($post_id, "product-flavors", $newflavors);
    }

    $oldfootimg = get_post_meta($post_id, "product-foot-img", true);
    $newfootimg = $_POST["product-foot-img"]; 
    if ($newfootimg != $oldfootimg) {
        update_post_meta($post_id, "product-foot-img", $newfootimg);
    }

    $oldhidetitle = get_post_meta($post_id, "product-hide-title", true);
    $newhidetitle = $_POST["product-hide-title"]; 
    if ($newhidetitle != $oldhidetitle) {
        update_post_meta($post_id, "product-hide-title", $newhidetitle);
    }

    $oldcarouselimg = get_post_meta($post_id, "product-carousel-img", true);
    $newcarouselimg = $_POST["product-carousel-img"]; 
    if ($newcarouselimg != $oldcarouselimg) {
        update_post_meta($post_id, "product-carousel-img", $newcarouselimg);
    }
}
add_action('save_post', 'spyral_product_save_data');
