<?php
// Custom post type
add_action( 'init', 'spyral_custom_post_type_branch', 0 );
function spyral_custom_post_type_branch() {
	$labels = array(
		'name'                => __( 'Sucursales' ),
		'singular_name'       => __( 'Sucursal'),
		'menu_name'           => __( 'Sucursales'),
		'parent_item_colon'   => __( 'Sucursal padre'),
		'all_items'           => __( 'Todas las Sucursales'),
		'view_item'           => __( 'Ver Sucursal'),
		'add_new_item'        => __( 'Agregar Nueva Sucursal'),
		'add_new'             => __( 'Agregar Sucursal'),
		'edit_item'           => __( 'Editar Sucursal'),
		'update_item'         => __( 'Actualizar Sucursal'),
		'search_items'        => __( 'Buscar Sucursal'),
		'not_found'           => __( 'Sucursal no encontrada'),
		'not_found_in_trash'  => __( 'Sucursal no encontrada en papelera')
	);
	$args = array(
		'label'               => __( 'spyral-branch'),
		'description'         => __( 'Sucursales Spyral'),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'public'              => false,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'has_archive'         => false,
		'can_export'          => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'rewrite'             => array( 'slug' => 'sucursal' ),
        'menu_icon'           => 'dashicons-location'
    );
	register_post_type( 'spyral-branch', $args );
}

// Add assets
function spyral_branch_assets() {
    $screen = get_current_screen();
    if($screen->id == 'spyral-branch'){
        $spyral_settings = get_option('spyral_settings');
	    $gmaps_apikey = $spyral_settings && $spyral_settings['gmaps_apikey'] ? $spyral_settings['gmaps_apikey'] : '';

        wp_enqueue_script( 'google-maps-native', 'http://maps.googleapis.com/maps/api/js?key='.$gmaps_apikey.'&libraries=places');
        wp_enqueue_script( 'map-script', get_template_directory_uri().'/admin/js/map.js');
    }
}
add_action( 'admin_enqueue_scripts', 'spyral_branch_assets' );

// Create Metabox
function spyral_branch_add_embed_gmaps_meta_box() {
    add_meta_box(
        'gmaps_embed_meta_box', // $id
        'Ubicación', // $title
        'spyral_branch_show_embed_gmaps_meta_box', // $callback
        'spyral-branch', // $page
        'normal', // $context
        'high'); // $priority
}
add_action('add_meta_boxes', 'spyral_branch_add_embed_gmaps_meta_box');

// Show Metabox Contents
function spyral_branch_show_embed_gmaps_meta_box() {
    global $post;  
	$lat = get_post_meta($post->ID, 'branch-lat', true);  
	$lng = get_post_meta($post->ID, 'branch-lng', true); 
	$street = get_post_meta($post->ID, 'branch-street-address', true); 
	$city = get_post_meta($post->ID, 'branch-city', true); 
	$state = get_post_meta($post->ID, 'branch-state', true); 
	$zip = get_post_meta($post->ID, 'branch-zip', true); 
	
	$nonce = wp_create_nonce(basename(__FILE__));
?>
<style>
#map-canvas {
	height: 50vh;
	width: 100%;
}
#branch-map-data input{
    width: 100%;
}
#branch-map-data textarea{
    width: 100%;
}

#map-search input, #map-search button{
    width: 100%;
}
</style>
<table style="width: 100%;">
    <tbody class="form-table" id="map-search">
        <tr>
            <th style="font-weight:normal">
                <input id="map-search-input" type="text" placeholder="Buscar en el mapa" />
            </th>
            <td style="width:20%; text-align: center">
                <button class="button button-primary" id="map-search-button">Buscar</button>
            </td>
        </tr>
    </tbody>
</table>

<div class="maparea" id="map-canvas"></div>
<input type="hidden" name="branch_map_meta_box_nonce" value="<?php echo $nonce; ?>">
<table style="width: 100%;">
    <tbody class="form-table" id="branch-map-data">
        <tr>
            <th style="font-weight:normal">
                <label for="branch-lat"><?php _e( 'Latitud', 'spyral-theme' )?></label>
            </th>
            <td>
                <input class="location-input" name="branch-lat" type="text" id="branch-lat" value="<?php echo $lat; ?>" />
            </td>
        </tr>
        <tr>
            <th style="font-weight:normal">
                <label for="branch-lng"><?php _e( 'Longitud', 'spyral-theme' )?></label>
            </th>
            <td>
                <input class="location-input" name="branch-lng" type="text" id="branch-lng" value="<?php echo $lng; ?>" />
            </td>
        </tr>
        <tr>
            <th style="font-weight:normal">
                <label for="branch-street-address"><?php _e( 'Calle', 'spyral-theme' )?></label>
            </th>
            <td>
                <textarea name="branch-street-address" id="branch-street-address" rows="10"><?php echo $street; ?></textarea>
            </td>
        </tr>
        <tr>
            <th style="font-weight:normal">
                <label for="branch-city"><?php _e( 'Ciudad', 'spyral-theme' )?></label>
            </th>
            <td>
                <input name="branch-city" type="text" id="branch-city" value="<?php echo $city; ?>" />
            </td>
        </tr>
        <tr>
            <th style="font-weight:normal">
                <label for="branch-state"><?php _e( 'Estado', 'spyral-theme' )?></label>
            </th>
            <td>
                <input name="branch-state" type="text" id="branch-state" value="<?php echo $state; ?>" />
            </td>
        </tr>
        <tr>
            <th style="font-weight:normal">
                <label for="branch-zip"><?php _e( 'Código Postal', 'spyral-theme' )?></label>
            </th>
            <td>
                <input name="branch-zip" type="text" id="branch-zip" value="<?php echo $zip; ?>" />
            </td>
        </tr>
    </tbody>
</table>
<?php
}

// Save Metaboxes.
function spyral_branch_save_embed_gmap($post_id) {   
    // verify nonce
    if (!wp_verify_nonce($_POST['branch_map_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }  
    
    $oldlat = get_post_meta($post_id, "branch-lat", true);
    $newlat = $_POST["branch-lat"]; 
    if ($newlat != $oldlat) {
        update_post_meta($post_id, "branch-lat", $newlat);
    } 

    $oldlng = get_post_meta($post_id, "branch-lng", true);
    $newlng = $_POST["branch-lng"]; 
    if ($newlng != $oldlng) {
        update_post_meta($post_id, "branch-lng", $newlng);
    }
    
    $oldstreet = get_post_meta($post_id, "branch-street-address", true);
    $newstreet = $_POST["branch-street-address"]; 
    if ($newstreet != $oldstreet) {
        update_post_meta($post_id, "branch-street-address", $newstreet);
    }

    $oldcity = get_post_meta($post_id, "branch-city", true);
    $newcity = $_POST["branch-city"]; 
    if ($newcity != $oldcity) {
        update_post_meta($post_id, "branch-city", $newcity);
    }

    $oldstate = get_post_meta($post_id, "branch-state", true);
    $newstate = $_POST["branch-state"]; 
    if ($newstate != $oldstate) {
        update_post_meta($post_id, "branch-state", $newstate);
    }

    $oldzip = get_post_meta($post_id, "branch-zip", true);
    $newzip = $_POST["branch-zip"]; 
    if ($newzip != $oldzip) {
        update_post_meta($post_id, "branch-zip", $newzip);
    }


}
add_action('save_post', 'spyral_branch_save_embed_gmap');