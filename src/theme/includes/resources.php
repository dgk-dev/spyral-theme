<?php
add_theme_support('post-thumbnails');

add_action('wp_enqueue_scripts', 'spyral_resources');
function spyral_resources(){
	//styles
	wp_enqueue_style('style', get_stylesheet_uri());

	//scripts
	$spyral_settings = get_option('spyral_settings');
	$gmaps_apikey = $spyral_settings && $spyral_settings['gmaps_apikey'] ? $spyral_settings['gmaps_apikey'] : '';
	wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?&key=' . $gmaps_apikey . '&libraries=places', array(), '', true);
	wp_enqueue_script('googlemaps');
	wp_enqueue_script('footer_js', get_template_directory_uri() . '/js/footer-bundle.js', null, 1.0, true);
	wp_localize_script('footer_js', 'spyralGlobalObject', array(
		'ajax_url' => admin_url('admin-ajax.php'),
		'theme_url' => get_template_directory_uri()
	));
}

//adds preloads
add_action('wp_head', 'spyral_preloads', 10);
function spyral_preloads(){
	$preload_fonts = '' .
		'<link rel="preload" href="' . get_template_directory_uri() . '/fonts/montserrat/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2" as="font" type="font/woff" crossorigin>' .
		'<link rel="preload" href="' . get_template_directory_uri() . '/fonts/montserrat/JTURjIg1_i6t8kCHKm45_ZpC3gnD_vx3rCs.woff2" as="font" type="font/woff" crossorigin>' .
		'<link rel="preload" href="' . get_template_directory_uri() . '/fonts/montserrat/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2" as="font" type="font/woff" crossorigin>' .
		'<link rel="preload" href="' . get_template_directory_uri() . '/fonts/poetsenone-regular-webfont.woff2" as="font" type="font/woff" crossorigin>';
	echo $preload_fonts;
}

//adds analytics
add_action('wp_head', 'spyral_analytics', 10);
function spyral_analytics(){
	$spyral_settings = get_option('spyral_settings');
	$analytics_code = $spyral_settings && $spyral_settings['analytics_code'] ? $spyral_settings['analytics_code'] : '';
	$add_view_product_event = $spyral_settings && $spyral_settings['analytics_add_view_product_event'] ? true : false;
	if ($analytics_code) {
		wp_localize_script('footer_js', 'spyralAnalyticsGlobalObject', array(
			'add_view_product_event' => $add_view_product_event
		));
		echo $analytics_code;
	}
}
