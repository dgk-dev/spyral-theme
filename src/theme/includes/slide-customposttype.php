<?php
// Custom post type
add_action('init', 'spyral_custom_post_type_slide', 0);
function spyral_custom_post_type_slide()
{
    $labels = array(
        'name'                => __('Slides'),
        'singular_name'       => __('Slide'),
        'menu_name'           => __('Slides'),
        'parent_item_colon'   => __('Slide padre'),
        'all_items'           => __('Todos los Slides'),
        'view_item'           => __('Ver Slide'),
        'add_new_item'        => __('Agregar Nuevo Slide'),
        'add_new'             => __('Agregar Slide'),
        'edit_item'           => __('Editar Slide'),
        'update_item'         => __('Actualizar Slide'),
        'search_items'        => __('Buscar Slide'),
        'not_found'           => __('Slide no encontrado'),
        'not_found_in_trash'  => __('Slide no encontrado en papelera')
    );
    $args = array(
        'label'               => __('spyral-branch'),
        'description'         => __('Slides Spyral'),
        'labels'              => $labels,
        'supports'            => array('title'),
        'public'              => false,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => false,
        'has_archive'         => false,
        'can_export'          => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
        'rewrite'             => array('slug' => 'slide'),
        'menu_icon'           => 'dashicons-images-alt2'
    );
    register_post_type('spyral-slide', $args);
}

// Add assets
function spyral_slide_assets() {
    $screen = get_current_screen();
    if($screen->id == 'spyral-slide'){
        wp_enqueue_script( 'media-selector-script', get_template_directory_uri().'/admin/js/media-selector.js');
    }
}
add_action( 'admin_enqueue_scripts', 'spyral_slide_assets' );

// Create Metabox
function spyral_slide_add_imgs_meta_box()
{
    add_meta_box(
        'slide_add_imgs_meta_box', // $id
        'Imágenes', // $title
        'spyral_slide_show_imgs_meta_box', // $callback
        'spyral-slide', // $page
        'normal', // $context
        'high'
    ); // $priority
}
add_action('add_meta_boxes', 'spyral_slide_add_imgs_meta_box');

// Show Metabox Contents
function spyral_slide_show_imgs_meta_box()
{
    wp_enqueue_media();
    global $post;
    $imgDesk = get_post_meta( $post->ID, 'slide-img-desk', true );
    $imgMob = get_post_meta( $post->ID, 'slide-img-mob', true );
    $srcDesk = wp_get_attachment_url( $imgDesk );
    $srcMob = wp_get_attachment_url( $imgMob );
    $nonce = wp_create_nonce(basename(__FILE__));
?>
    <input type="hidden" name="spyral_slide_meta_box_nonce" value="<?php echo $nonce; ?>">
    <table style="width: 100%;">
        <tbody class="form-table">
            <tr>
                <th>
                    <label for="slide-img-desk"><?php _e('Imagen desk', 'spyral-theme') ?></label>
                </th>
                <td>
                    <input type="hidden" name="slide-img-desk" id="slide-img-desk" value="<?php echo esc_attr($imgDesk); ?>">
                    <button type="button" class="button media-selector" data-media-uploader-target="#slide-img-desk" data-img-target="#thumbnail-desk"><?php _e('Elegir archivo', 'spyral-theme') ?></button>
                    <img style="max-width: 200px; margin: 0 0 10px 0; display:block;" id="thumbnail-desk" src="<?php echo esc_attr($srcDesk); ?>" alt="">
                </td>
            </tr>
            <tr>
                <th>
                    <label for="slide-img-mob"><?php _e('Imagen mobile', 'spyral-theme') ?></label>
                </th>
                <td>
                    <input type="hidden" name="slide-img-mob" id="slide-img-mob" value="<?php echo esc_attr($imgMob); ?>">
                    <button type="button" class="button media-selector" data-media-uploader-target="#slide-img-mob" data-img-target="#thumbnail-mob"><?php _e('Elegir archivo', 'spyral-theme') ?></button>
                    <img style="max-width: 200px; margin: 0 0 10px 0; display:block;" id="thumbnail-mob" src="<?php echo esc_attr($srcMob); ?>" alt="">
                </td>
            </tr>
        </tbody>
    </table>
<?php
}

// // Save Metaboxes.
function spyral_slide_save_imgs($post_id) {   
    // verify nonce
    if (!wp_verify_nonce($_POST['spyral_slide_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }  
    
    $oldimgdesk = get_post_meta($post_id, "slide-img-desk", true);
    $newimgdesk = $_POST["slide-img-desk"]; 
    if ($newimgdesk != $oldimgdesk) {
        update_post_meta($post_id, "slide-img-desk", $newimgdesk);
    }

    $oldimgmob = get_post_meta($post_id, "slide-img-mob", true);
    $newimgmob = $_POST["slide-img-mob"]; 
    if ($newimgmob != $oldimgmob) {
        update_post_meta($post_id, "slide-img-mob", $newimgmob);
    } 

}
add_action('save_post', 'spyral_slide_save_imgs');
