<?php
//maps function
//AJAX load equipment
function spyral_load_markers(){
	$places = array();
	$bounds = isset($_POST['bounds']) ? $_POST['bounds'] : array();
	
	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'spyral-branch' ),
		'post_status'            => array( 'publish' ),
		'posts_per_page'         => '1'
	);

	if(!empty($bounds)){
		$args['meta_query'] = array(
			array(
			  'key'     => 'branch-lat',
			  'value'   => array( $bounds['south'], $bounds['north']),
			  'compare' => 'BETWEEN',
			  'type'    => 'DECIMAL', 
			),
			array(
			  'key'     => 'branch-lng',
			  'value'   => array( $bounds['west'], $bounds['east']),
			  'compare' => 'BETWEEN',
			  'type'    => 'DECIMAL', 
			)
		);
		$args['posts_per_page'] = -1;
	}

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$placeData = array(
				'title' => get_the_title(),
				'address' => get_post_meta( get_the_ID() , 'branch-street-address', true ),
				'lat' => get_post_meta( get_the_ID() , 'branch-lat', true ),
				'lng' => get_post_meta( get_the_ID() , 'branch-lng', true ),
			);
			if(current_user_can('administrator')){
				$placeData['edit'] = '<a href="'.get_edit_post_link(get_the_ID()).'" target="BLANK">Editar</a>';
			}
			$places[] = $placeData;
		}
	}
	echo wp_json_encode($places); wp_die();
}

add_action('wp_ajax_spyral_load_markers', 'spyral_load_markers');
add_action('wp_ajax_nopriv_spyral_load_markers', 'spyral_load_markers');

add_filter('get_meta_sql','spyral_cast_decimal_precision');
function spyral_cast_decimal_precision( $array ) {

    $array['where'] = str_replace('DECIMAL','DECIMAL(10,3)',$array['where']);

    return $array;
}