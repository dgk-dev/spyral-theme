
//t = current time
//b = start value
//c = change in value
//d = duration
export const easeInOutQuad = (t, b, c, d) => {
  t /= d / 2;
  if (t < 1) return c / 2 * t * t + b;
  t--;
  return -c / 2 * (t * (t - 2) - 1) + b;
};

export const scrollTo = (element, to, duration = 600) => {
  let start = element.scrollTop || element.pageYOffset,
      change = to - start,
      currentTime = 0,
      increment = 20;
        
  const animateScroll = function() {        
    currentTime += increment;
    let val = easeInOutQuad(currentTime, start, change, duration);
    
    element.scroll(0, val);

    if(currentTime < duration) {
      setTimeout(animateScroll, increment);
    }
  };

  animateScroll();
};


export default {
  init (elements) {
    this.elements = typeof elements.navLinks !== 'undefined' ? [...elements.navLinks] : [...elements];
    this.mobileNavButton = typeof elements.mobileNavButton !== 'undefined' ? elements.mobileNavButton : null;
    this.mobileContainer = typeof elements.mobileContainer !== 'undefined' ? elements.mobileContainer : null;
    this.mobileContainerclassName = `${this.mobileContainer.className.split(' ')[0]}--active`;
    this.navBreakpoint = typeof elements.navBreakpoint !== 'undefined' ? elements.navBreakpoint : null;

    this.moveTo = this.moveTo.bind(this);
    this.toggleNav = this.toggleNav.bind(this);
    
    this.bindEvents();
  },

  bindEvents () {
    this.elements.forEach(element => {
      element.addEventListener('click', this.moveTo, false);
    });
    if (this.mobileNavButton !== null) {
      this.mobileNavButton.addEventListener('click', this.toggleNav, false);
    }
  },

  moveTo (e) {
    const href = e.currentTarget.href;
    const link = href.indexOf('#') > -1 ? href.split('#')[1] : null;

    if (link !== null) {
      e.preventDefault();
      scrollTo(window, document.querySelector(`#${link}`).offsetTop);
    }

    if (window.innerWidth <= this.navBreakpoint && this.mobileContainer.classList.contains(this.mobileContainerclassName)) {
      this.toggleNav();
    }
  },

  toggleNav (e) {
    if (this.mobileContainer === null) return;

    if (this.mobileContainer.classList.contains(this.mobileContainerclassName)) {
      this.mobileContainer.classList.remove(this.mobileContainerclassName);
    } else {
      this.mobileContainer.classList.add(this.mobileContainerclassName);
    }
  }
};
