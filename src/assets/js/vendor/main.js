import Swiper from 'swiper';

import nav from './scrollTo';
import detail from './detail';
import registerSW from './sw-register';


const slideshow = () => {
  const slideshowSwiper = new Swiper('.slideshow', {
    //grabCursor: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
};

const carousel = () => {
  const carouselSwiper = new Swiper('.carousel', {
    slidesPerView: 4.7,
    spaceBetween: 35,
    centeredSlides: true,
    //grabCursor: true,
    loop: true,
    navigation: {
      nextEl: '.carousel__button--next',
      prevEl: '.carousel__button--prev',
    },
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 2.1,
        spaceBetween: 25
      },
      // when window width is >= 768px
      768: {
        slidesPerView: 3.8,
        spaceBetween: 30
      },
      // when window width is >= 1024px
      1024: {
        slidesPerView: 4.7,
        spaceBetween: 35
      }
    }
  });
};

const init = () => {
  nav.init({
    mobileContainer: document.querySelector('.mov-container'),
    navLinks: [...document.querySelectorAll('.main-n__link'), ...document.querySelectorAll('.main-f__link')],
    mobileNavButton: document.querySelector('.toggle-nav'),
    navBreakpoint: 1024
  });
  slideshow();
  carousel();
  detail(
    document.querySelectorAll('.carousel__slide'), // list to click
    document.querySelector('.detail'), // element to insert data
    [document.querySelector('.slideshow')] // elements to hide on click
  );
};

window.onload = init;
// registerSW();
