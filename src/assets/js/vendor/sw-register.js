

export default function () {

  const registerSW = () => {
    navigator.serviceWorker.register('../../sw.js')
      .then(registration => {
      })
      .catch(error => {
        console.error('Error during service worker registration:', error);
      });
  };

  if ('serviceWorker' in navigator) {
    window.addEventListener('load', registerSW);
  }
};
