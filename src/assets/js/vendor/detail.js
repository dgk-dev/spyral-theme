export default (detailList, detailView, sectionsToHide) => {
  const carouselList = [...detailList];
  detailView.style.display = 'none';

  const constructHtml = (data) => {
    return `
      <div class="container flex detail__wrapper">
        <div class="detail__info">
          <h1 class="detail__title">${data.title}</h1>
          <p class="detail__text">
          ${data.text}
          </p>
          <ul class="detail__list">
          ${data.list.map(item => `<li>${item}</li>`).join('')}
          </ul>

          <figure class="detail__extra-image">
            <img src="${data.imgFooter}" alt="Spyral - ${data.title}">
          </figure>
        </div>
        
        <div class="detail__main-image">
          <figure>
            <image src="${data.img}" alt="Spyral - ${data.title}" />
          </figure>
        </div>
      </div>
    `;
  };

  const handleItemDetail = (e) => {
    e.preventDefault();

    const { target } = e;
    let currentTarget = null;

    if (target.tagName === 'H3' || target.tagName === 'FIGURE') {
      currentTarget = target.parentElement;
    } else if (target.tagName === 'IMG') {
      currentTarget = target.parentElement.parentElement;
    } else if (target.tagName === 'A') {
      currentTarget = target;
    }
          
    if (currentTarget === null) return;
    
    const { dataset } = currentTarget;
    const { text, list, img, imgFooter, color } = dataset;
    const data = {
      title: currentTarget.querySelector('h3').innerText, 
      text, 
      list: list.split(','), 
      img,
      imgFooter
    };

    sectionsToHide.forEach(section => {
      section.style.display = 'none';
    });
    window.scroll(0, 0);
    detailView.innerHTML = constructHtml(data);
    detailView.className = `detail detail--${color}`;
    detailView.style.display = 'block';

    // Google Tag Manager event
    if(spyralAnalyticsGlobalObject.add_view_product_event){
      gtag('event', 'view', {
        'event_category': 'Vista de producto',
        'event_label': data.title,
      });
    }
  };


  carouselList.forEach(item => {
    item.addEventListener('click', handleItemDetail, false);
  });
};
