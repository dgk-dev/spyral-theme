const CACHE_NAME = 'spyral-v1';
const STATIC_CACHE_URLS = [
  '/',
  '/site.manifest',
  '/favicon.ico',
  '/favicon-16x16.png',
  '/favicon-32x32.png',
  '/public/fonts/montserrat/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2',
  '/public/fonts/montserrat/JTURjIg1_i6t8kCHKm45_ZpC3gnD_vx3rCs.woff2',
  '/public/fonts/montserrat/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2',
  '/public/fonts/poetsenone-regular-webfont.woff2',
  '/public/css/main.min.css',
  '/public/js/bundle.min.js',
  '/public/images/shape.svg',
  '/public/images/shape-02@2x.png',
  '/public/images/lines@2x.png',
  '/public/images/shape-3@2x.png',
  '/public/images/shape-4.svg',
  '/public/images/puntos@2x.png',
  '/public/images/cono2@2x.png',
  '/public/images/cono2.png',
  '/public/images/cono@2x.png',
  '/public/images/cono.png',
  '/public/images/sundae@2x.png',
  '/public/images/sundae.png',
  '/public/images/img-cono-sencillo@2x.png',
  '/public/images/img-cono-sencillo.png',
  '/public/images/img-conos-max@2x.png',
  '/public/images/img-conos-max.png',
  '/public/images/img-conos-gigante@2x.png',
  '/public/images/img-conos-gigante.png',
  '/public/images/img-malteada@2x.png',
  '/public/images/img-malteada.png',
  '/public/images/sundae2@2x.png',
  '/public/images/sundae2.png'
];

const onInstall = event => {
  // installing
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => cache.addAll(STATIC_CACHE_URLS))
      .catch(err => console.log('An error occurred during installation: ', err))
  );
};

const onActivate = event => {
  event.waitUntil(
    caches.keys()
      .then(keys => keys.filter(key => key !== CACHE_NAME))
      .then(keys => Promise.all(keys.map(key => caches.delete(key))))
  );
};

const onFetch = event => {
  if (event.request.method !== 'GET') {
    return;
  }

  const fetchedFromNetwork = response => {
    const cacheCopy = response.clone();

    caches.open(CACHE_NAME)
      .then(cache => {
        cache.put(event.request, cacheCopy);
      });

    return response;
  };

  const unableToResolve = () => {
    return new Response('<h1>Service Unavailable</h1>', {
      status: 503,
      statusText: 'Service Unavailable',
      headers: new Headers({
        'Content-Type': 'text/html'
      })
    });
  };

  event.respondWith(
    caches.match(event.request)
      .then(cached => {
        const networked = fetch(event.request)
          .then(fetchedFromNetwork, unableToResolve)
          .catch(unableToResolve);

        return cached || networked;
      })
  );
};

self.addEventListener('install', onInstall);
self.addEventListener('activate', onActivate);
self.addEventListener('fetch', onFetch);
