function initSpyralMap() {
  var map, infoWindow, geolocation, timer, markersArray = [], loadingMarkers, infoWindow, locationMarker;
  var markerIcons = ['purple_marker', 'orange_marker', 'blue_marker', 'green_marker', 'yellow_marker'];

  geolocation = {
    lat: 19.432608,
    lng: -99.133209
  }

  map = new google.maps.Map(document.getElementById('spyral-map'), {
    center: geolocation,
    zoom: 12,
    mapTypeControl: false,
    fullscreenControl: false
  });

  //infowindow
  infoWindow = new google.maps.InfoWindow();
  google.maps.event.addListener(infoWindow, 'domready', function () {
    $('.gm-ui-hover-effect').addClass(this.getAnchor().infoColor);
  });

  //Geocoder
  var geocoder = new google.maps.Geocoder();

  //event listeners
  map.addListener('idle', function () {
    clearTimeout(timer);
    timer = setTimeout(loadMarkers(), 2000);
  });

  $('#spyral-map-search').submit(function (e) {
    e.preventDefault();
    var searchLocation = $(this).find('input[name=location]').val();
    if (!searchLocation) return false;
    geocoder.geocode({ 'address': searchLocation }, function (results, status) {
      if (status == 'OK') {
        map.setCenter(results[0].geometry.location);
        map.fitBounds(results[0].geometry.viewport);
      }
    });
  });

  // My location button
  var myLocationControlDiv = document.createElement('div');
  var myLocationControl = new MyLocationControl(myLocationControlDiv, map);

  myLocationControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(myLocationControlDiv);


  function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        geolocation.lat = position.coords.latitude;
        geolocation.lng = position.coords.longitude;
        map.setCenter(geolocation);
        map.setZoom(16);

        if (locationMarker == undefined) {
          locationMarker = new LocationMarker(geolocation.lat, geolocation.lng);
          locationMarker.setMap(map);
        } else {
          locationMarker.pos = new google.maps.LatLng(geolocation.lat, geolocation.lng);
          locationMarker.draw();
        }
        $('#my-location-control').addClass('active');
      });
    } else {
      console.log('Browser doesn\'t support geolocation');
    }
  }

  function loadMarkers() {
    if (!loadingMarkers) {
      $.ajax({
        type: 'post',
        url: spyralGlobalObject.ajax_url,
        data: {
          action: 'spyral_load_markers',
          bounds: map.getBounds().toJSON()
        },
        dataType: 'json',
        success: function (markers) {
          $.each(markers, function (index, value) {
            var markerLatlng = new google.maps.LatLng(value.lat, value.lng);

            if (!isLocationFree(markerLatlng)) return;

            var markerIconIndex = Math.floor(Math.random() * markerIcons.length);
            var marker = new google.maps.Marker({
              position: markerLatlng,
              title: value.title,
              animation: google.maps.Animation.DROP,
              address: value.address,
              edit: value.edit ? value.edit : 0,
              icon: spyralGlobalObject.theme_url + '/img/' + markerIcons[markerIconIndex] + '.svg',
              infoColor: markerIcons[markerIconIndex]
            });

            marker.setMap(map);
            markersArray.push(marker);

            google.maps.event.addListener(marker, 'click', function () {
              infoWindow.close(); // Close previously opened infowindow
              infoWindow.setContent(setInfoWindowContent(this));
              infoWindow.open(map, this);
            });
          });
        },
        error: function (response) {
          console.log('error', response);
        },
        beforeSend: function (qXHR, settings) {
          loadingMarkers = true;
          clearOutboundMarkers();
        },
        complete: function () {
          loadingMarkers = false;
        }
      });
    }
  }

  function isLocationFree(gLatLng) {
    for (var i = 0; i < markersArray.length; i++) {
      if (
        markersArray[i].position.lat() === gLatLng.lat()
        &&
        markersArray[i].position.lng() === gLatLng.lng()
      ) return false;
    }
    return true;
  }

  function clearOutboundMarkers() {
    var bounds = map.getBounds().toJSON();
    for (var i = 0; i < markersArray.length; i++) {
      if (
        bounds.north < markersArray[i].position.lat() || bounds.south > markersArray[i].position.lat()
        ||
        bounds.east < markersArray[i].position.lng() || bounds.west > markersArray[i].position.lng()
      ) {
        markersArray[i].setMap(null);
        markersArray.splice(i, 1);
      }
    }
  }

  function setInfoWindowContent(marker) {
    var contentString =
      '<div id="infowindow-content" class="' + marker.infoColor + '">' +
      '<div id="infowindow-content__data">' +
      '<h3 class="title">' + marker.title + '</h3>' +
      '<p>' + marker.address + '</p>' +
      '<a class="g-maps-link" target="_BLANK" href="http://www.google.com/maps/place/' + marker.getPosition().lat() + ',' + marker.getPosition().lng() + '">Abrir en Google Maps</a>' +
      '</div>' +
      '<div id="infowindow-content__logo">' +
      '<img src="' + spyralGlobalObject.theme_url + '/img/logo_spyral_round.png"/>' +
      '</div>' +
      '</div>';

    if (marker.edit) {
      contentString += marker.edit;
    }

    return contentString;
  }

  //Location button
  function MyLocationControl(controlDiv, map) {
    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.id = 'my-location-control';
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '2px';
    controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.3) 0px 1px 4px -1px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginRight = '10px';
    controlUI.style.textAlign = 'center';
    controlUI.style.width = '40px';
    controlUI.style.height = '40px';
    controlUI.style.boxSizing = 'border-box';
    controlUI.style.padding = '5px';
    controlUI.title = 'Mi ubicación';
    controlDiv.appendChild(controlUI);
  
    // Set CSS for the control interior.
    var controlIcon = document.createElement('div');
    controlIcon.id = 'my-location-control__icon';
    controlUI.appendChild(controlIcon);
  
    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function() {
      geolocate();
    });
  }
  
  //set location marker overlay
  function LocationMarker(lat, lng) {
    this.lat = lat;
    this.lng = lng;
    this.pos = new google.maps.LatLng(this.lat, this.lng);
  }

  LocationMarker.prototype = new google.maps.OverlayView();
  LocationMarker.prototype.onRemove = function () { }

  LocationMarker.prototype.onAdd = function () {
    var span = document.createElement('SPAN');
    span.className = "location-pulse";
    var panes = this.getPanes();
    panes.overlayImage.appendChild(span);
  }

  LocationMarker.prototype.draw = function () {
    var overlayProjection = this.getProjection();
    var position = overlayProjection.fromLatLngToDivPixel(this.pos);
    var panes = this.getPanes();
    panes.overlayImage.style.left = position.x + 'px';
    panes.overlayImage.style.top = position.y - 30 + 'px';
  }
}
initSpyralMap();